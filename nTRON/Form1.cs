﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nTRON
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            g = CreateGraphics();
            DoubleBuffered = true;
            gamer = new LightBike(new Point(10, 30), new Size(15, 15), Color.LightBlue);
            enemy1 = new LightBike(new Point(this.Width-40, 30), new Size(15, 15), Color.Red);
            enemy1.VectLeft();
            mas = new int[(this.Width + 120), (this.Height + 120)];
        }

        Graphics g;
        LightBike gamer, enemy1;
        int[,] mas;
        bool game = true;

        private void timer1_Tick(object sender, EventArgs e)
        {
            int r;
            Random rnd = new Random();
            if (gamer.live)
            {
                #region BikerLiveCode
                mas[(gamer.bike.X + gamer.bike.Width / 2), (gamer.bike.Y + gamer.bike.Height / 2)] = 1;
                switch (gamer.dir)
                {
                    case 1:
                        if (mas[(gamer.bike.X + gamer.bike.Width / 2), (gamer.bike.Y + gamer.bike.Height / 2) - 1] == 1)
                            gamer.live = false;
                        break;
                    case 2:
                        if (mas[(gamer.bike.X + gamer.bike.Width / 2 + 1), (gamer.bike.Y + gamer.bike.Height / 2)] == 1)
                            gamer.live = false;
                        break;
                    case 3:
                        if (mas[(gamer.bike.X + gamer.bike.Width / 2), (gamer.bike.Y + gamer.bike.Height / 2) + 1] == 1)
                            gamer.live = false;
                        break;
                    case 4:
                        if (mas[(gamer.bike.X + gamer.bike.Width / 2 - 1), (gamer.bike.Y + gamer.bike.Height / 2)] == 1)
                            gamer.live = false;
                        break;
                }
                if (gamer.bike.X > this.Width || gamer.bike.X < 0)
                    gamer.live = false;
                if (gamer.bike.Y > this.Height || gamer.bike.Y < 0)
                    gamer.live = false;
                gamer.Update();
                gamer.BikeDraw(g);
                #endregion
            }
            if (enemy1.live)
            {
                r = rnd.Next(98);
                
                #region Enemy1LiveCode

                mas[(enemy1.bike.X + enemy1.bike.Width / 2), (enemy1.bike.Y + enemy1.bike.Height / 2)] = 1;
                #region Enemy1DirSwitchDeath
                switch (enemy1.dir)
                {
                    case 1:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2), (enemy1.bike.Y + enemy1.bike.Height / 2) - 1] == 1)
                            enemy1.live = false;
                        break;
                    case 2:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2 + 1), (enemy1.bike.Y + enemy1.bike.Height / 2)] == 1)
                            enemy1.live = false;
                        break;
                    case 3:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2), (enemy1.bike.Y + enemy1.bike.Height / 2) + 1] == 1)
                            enemy1.live = false;
                        break;
                    case 4:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2 - 1), (enemy1.bike.Y + enemy1.bike.Height / 2)] == 1)
                            enemy1.live = false;
                        break;
                }
                #endregion
                #region Enemy1DirSwitchTurn
                switch (enemy1.dir)
                {
                    case 1:
                        if (r > 97)
                            enemy1.VectRight();
                        else if (r > 92)
                            enemy1.VectLeft();
                        break;
                    case 2:
                        if (r > 96)
                            enemy1.VectUp();
                        else if (r > 92)
                            enemy1.VectDown();
                        break;
                    case 3:
                        if (r > 96)
                            enemy1.VectRight();
                        else if (r > 92)
                            enemy1.VectLeft();
                        break;
                    case 4:
                        if (r > 96)
                            enemy1.VectDown();
                        else if (r > 92)
                            enemy1.VectUp();
                        break;
                }
                #endregion
                #region Enemy1DirSwitchTurn2
                switch (enemy1.dir)
                {
                    case 1:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2), (enemy1.bike.Y + enemy1.bike.Height / 2) - 15] == 1 || enemy1.bike.Y < 10)
                            enemy1.VectLeft();
                        break;
                    case 2:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2 + 15), (enemy1.bike.Y + enemy1.bike.Height / 2)] == 1 || enemy1.bike.X < 10)
                            enemy1.VectDown();
                        break;
                    case 3:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2), (enemy1.bike.Y + enemy1.bike.Height / 2) + 15] == 1 || enemy1.bike.Y > this.Height - 20)
                            enemy1.VectRight();
                        break;
                    case 4:
                        if (mas[(enemy1.bike.X + enemy1.bike.Width / 2 - 15), (enemy1.bike.Y + enemy1.bike.Height / 2)] == 1 || enemy1.bike.X > this.Width - 20)
                            enemy1.VectUp();
                        break;
                }
                #endregion
                if (enemy1.bike.X > this.Width || enemy1.bike.X < 0)
                    enemy1.live = false;
                if (enemy1.bike.Y > this.Height || enemy1.bike.Y < 0)
                    enemy1.live = false;
                enemy1.Update();
                enemy1.BikeDraw(g);
                #endregion
            }
            if (!gamer.live)
            {
                timer1.Stop();
                MessageBox.Show("Gamer 1, you're LOSER! Gamer 2 wins.", "Game Over!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Restart();
            }
            if (!enemy1.live)
            {
                timer1.Stop();
                MessageBox.Show("Gamer 2 loses this game like a FOOL! Gamer 1 can take his prize.", "Game Over!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Restart();
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
                gamer.VectDown();
            if (e.KeyCode == Keys.W)
                gamer.VectUp();
            if (e.KeyCode == Keys.A)
                gamer.VectLeft();
            if (e.KeyCode == Keys.D)
                gamer.VectRight();
            
            /*if (e.KeyCode == Keys.Down)
                enemy1.VectDown();
            if (e.KeyCode == Keys.Up)
                enemy1.VectUp();
            if (e.KeyCode == Keys.Left)
                enemy1.VectLeft();
            if (e.KeyCode == Keys.Right)
                enemy1.VectRight();*/
        }
        #region MenuStrip
        #region GameMenuStrip
        private void паузаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (game)
            {
                timer1.Stop();
                паузаToolStripMenuItem.Text = "Продолжить";
                game = false;
                Console.WriteLine("Game");
            }
            else
            if (!game)
            {
                timer1.Start();
                паузаToolStripMenuItem.Text = "Пауза";
                game = true;
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void новаяИграToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        #endregion
        #region SettingsMenuStrip
        #region MenuSpeed
        private void menuSpeed2_Click(object sender, EventArgs e)
        {
            menuSpeed2.Checked = true;
            menuSpeed5.Checked = false;
            menuSpeed10.Checked = false;
            menuSpeed15.Checked = false; 
            menuSpeed20.Checked = false;
            timer1.Interval = 2;
        }

        private void menuSpeed5_Click(object sender, EventArgs e)
        {
            menuSpeed2.Checked = false;
            menuSpeed5.Checked = true;
            menuSpeed10.Checked = false;
            menuSpeed15.Checked = false;
            menuSpeed20.Checked = false;
            timer1.Interval = 5;
        }

        private void menuSpeed10_Click(object sender, EventArgs e)
        {
            menuSpeed2.Checked = false;
            menuSpeed5.Checked = false;
            menuSpeed10.Checked = true;
            menuSpeed15.Checked = false;
            menuSpeed20.Checked = false;
            timer1.Interval = 10;
        }

        private void menuSpeed15_Click(object sender, EventArgs e)
        {
            menuSpeed2.Checked = false;
            menuSpeed5.Checked = false;
            menuSpeed10.Checked = false;
            menuSpeed15.Checked = true;
            menuSpeed20.Checked = false;
            timer1.Interval = 15;
        }

        private void menuSpeed20_Click(object sender, EventArgs e)
        {
            menuSpeed2.Checked = false;
            menuSpeed5.Checked = false;
            menuSpeed10.Checked = false;
            menuSpeed15.Checked = false;
            menuSpeed20.Checked = true;
            timer1.Interval = 20;
        }
        #endregion
        #region PlayerColor
        private void Player_Click(object sender, EventArgs e)
        {
            if (sender.ToString() == "Красный")
            {
                gamer.bikecolor = Color.Red;
                PlayerRed.Checked = true;
                PlayerYellow.Checked = false;
                PlayerGreen.Checked = false;
                PlayerBlue.Checked = false;
            }
            if (sender.ToString() == "Жёлтый")
            {
                gamer.bikecolor = Color.Yellow;
                PlayerRed.Checked = false;
                PlayerYellow.Checked = true;
                PlayerGreen.Checked = false;
                PlayerBlue.Checked = false;
            }
            if (sender.ToString() == "Зелёный")
            {
                gamer.bikecolor = Color.LightGreen;
                PlayerRed.Checked = false;
                PlayerYellow.Checked = false;
                PlayerGreen.Checked = true;
                PlayerBlue.Checked = false;
            }
            if (sender.ToString() == "Синий")
            {
                gamer.bikecolor = Color.LightBlue;
                PlayerRed.Checked = false;
                PlayerYellow.Checked = false;
                PlayerGreen.Checked = false;
                PlayerBlue.Checked = true;
            }
        }
        private void PlayerYellow_Click(object sender, EventArgs e)
        {
            gamer = new LightBike(new Point(gamer.bike.X, gamer.bike.Y), new Size(15, 15), Color.Yellow);
            PlayerRed.Checked = false;
            PlayerYellow.Checked = true;
            PlayerGreen.Checked = false;
            PlayerBlue.Checked = false;
        }
        private void PlayerGreen_Click(object sender, EventArgs e)
        {
            gamer = new LightBike(new Point(gamer.bike.X, gamer.bike.Y), new Size(15, 15), Color.LightGreen);
            PlayerRed.Checked = false;
            PlayerYellow.Checked = false;
            PlayerGreen.Checked = true;
            PlayerBlue.Checked = false;
        }
        private void PlayerBlue_Click(object sender, EventArgs e)
        {
            gamer = new LightBike(new Point(gamer.bike.X, gamer.bike.Y), new Size(15, 15), Color.LightBlue);
            PlayerRed.Checked = false;
            PlayerYellow.Checked = false;
            PlayerGreen.Checked = false;
            PlayerBlue.Checked = true;
        }
        #endregion
        #region EnemyColor
        private void Enemy_Click(object sender, EventArgs e)
        {
            if (sender.ToString() == "Красный")
            {
                enemy1.bikecolor = Color.Red;
                EnemyRed.Checked = true;
                EnemyYellow.Checked = false;
                EnemyGreen.Checked = false;
                EnemyBlue.Checked = false;
            }
            if (sender.ToString() == "Жёлтый")
            {
                enemy1.bikecolor = Color.Yellow;
                EnemyRed.Checked = false;
                EnemyYellow.Checked = true;
                EnemyGreen.Checked = false;
                EnemyBlue.Checked = false;
            }
            if (sender.ToString() == "Зелёный")
            {
                enemy1.bikecolor = Color.LightGreen;
                EnemyRed.Checked = false;
                EnemyYellow.Checked = false;
                EnemyGreen.Checked = true;
                EnemyBlue.Checked = false;
            }
            if (sender.ToString() == "Синий")
            {
                enemy1.bikecolor = Color.LightBlue;
                EnemyRed.Checked = false;
                EnemyYellow.Checked = false;
                EnemyGreen.Checked = false;
                EnemyBlue.Checked = true;
            }
        }
        #endregion
        #endregion
        #endregion
    }
}
