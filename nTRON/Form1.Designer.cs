﻿namespace nTRON
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.играToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяИграToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.паузаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скоростьИгрыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpeed2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpeed5 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpeed10 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpeed15 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpeed20 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.цветИгрока1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayerRed = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayerYellow = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayerGreen = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayerBlue = new System.Windows.Forms.ToolStripMenuItem();
            this.цветИгрока2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EnemyRed = new System.Windows.Forms.ToolStripMenuItem();
            this.EnemyYellow = new System.Windows.Forms.ToolStripMenuItem();
            this.EnemyGreen = new System.Windows.Forms.ToolStripMenuItem();
            this.EnemyBlue = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.играToolStripMenuItem,
            this.настройкиToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 579);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1082, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // играToolStripMenuItem
            // 
            this.играToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяИграToolStripMenuItem,
            this.паузаToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.играToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.играToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Control;
            this.играToolStripMenuItem.Name = "играToolStripMenuItem";
            this.играToolStripMenuItem.Size = new System.Drawing.Size(57, 25);
            this.играToolStripMenuItem.Text = "Игра";
            // 
            // новаяИграToolStripMenuItem
            // 
            this.новаяИграToolStripMenuItem.Name = "новаяИграToolStripMenuItem";
            this.новаяИграToolStripMenuItem.Size = new System.Drawing.Size(160, 26);
            this.новаяИграToolStripMenuItem.Text = "Новая игра";
            this.новаяИграToolStripMenuItem.Click += new System.EventHandler(this.новаяИграToolStripMenuItem_Click);
            // 
            // паузаToolStripMenuItem
            // 
            this.паузаToolStripMenuItem.Name = "паузаToolStripMenuItem";
            this.паузаToolStripMenuItem.Size = new System.Drawing.Size(160, 26);
            this.паузаToolStripMenuItem.Text = "Пауза";
            this.паузаToolStripMenuItem.Click += new System.EventHandler(this.паузаToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(160, 26);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.скоростьИгрыToolStripMenuItem,
            this.toolStripSeparator1,
            this.цветИгрока1ToolStripMenuItem,
            this.цветИгрока2ToolStripMenuItem});
            this.настройкиToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.настройкиToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Control;
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(99, 25);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            // 
            // скоростьИгрыToolStripMenuItem
            // 
            this.скоростьИгрыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSpeed2,
            this.menuSpeed5,
            this.menuSpeed10,
            this.menuSpeed15,
            this.menuSpeed20});
            this.скоростьИгрыToolStripMenuItem.Name = "скоростьИгрыToolStripMenuItem";
            this.скоростьИгрыToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.скоростьИгрыToolStripMenuItem.Text = "Скорость игры";
            // 
            // menuSpeed2
            // 
            this.menuSpeed2.Name = "menuSpeed2";
            this.menuSpeed2.Size = new System.Drawing.Size(233, 26);
            this.menuSpeed2.Text = "2 (Очень быстро)";
            this.menuSpeed2.Click += new System.EventHandler(this.menuSpeed2_Click);
            // 
            // menuSpeed5
            // 
            this.menuSpeed5.Name = "menuSpeed5";
            this.menuSpeed5.Size = new System.Drawing.Size(233, 26);
            this.menuSpeed5.Text = "5 (Быстро)";
            this.menuSpeed5.Click += new System.EventHandler(this.menuSpeed5_Click);
            // 
            // menuSpeed10
            // 
            this.menuSpeed10.Checked = true;
            this.menuSpeed10.CheckState = System.Windows.Forms.CheckState.Checked;
            this.menuSpeed10.Name = "menuSpeed10";
            this.menuSpeed10.Size = new System.Drawing.Size(233, 26);
            this.menuSpeed10.Text = "10 (Стандарт)";
            this.menuSpeed10.Click += new System.EventHandler(this.menuSpeed10_Click);
            // 
            // menuSpeed15
            // 
            this.menuSpeed15.Name = "menuSpeed15";
            this.menuSpeed15.Size = new System.Drawing.Size(233, 26);
            this.menuSpeed15.Text = "15 (Медленно)";
            this.menuSpeed15.Click += new System.EventHandler(this.menuSpeed15_Click);
            // 
            // menuSpeed20
            // 
            this.menuSpeed20.Name = "menuSpeed20";
            this.menuSpeed20.Size = new System.Drawing.Size(233, 26);
            this.menuSpeed20.Text = "20 (Очень медленно)";
            this.menuSpeed20.Click += new System.EventHandler(this.menuSpeed20_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // цветИгрока1ToolStripMenuItem
            // 
            this.цветИгрока1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PlayerRed,
            this.PlayerYellow,
            this.PlayerGreen,
            this.PlayerBlue});
            this.цветИгрока1ToolStripMenuItem.Name = "цветИгрока1ToolStripMenuItem";
            this.цветИгрока1ToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.цветИгрока1ToolStripMenuItem.Text = "Цвет игрока 1";
            // 
            // PlayerRed
            // 
            this.PlayerRed.Name = "PlayerRed";
            this.PlayerRed.Size = new System.Drawing.Size(152, 26);
            this.PlayerRed.Text = "Красный";
            this.PlayerRed.Click += new System.EventHandler(this.Player_Click);
            // 
            // PlayerYellow
            // 
            this.PlayerYellow.Name = "PlayerYellow";
            this.PlayerYellow.Size = new System.Drawing.Size(152, 26);
            this.PlayerYellow.Text = "Жёлтый";
            this.PlayerYellow.Click += new System.EventHandler(this.Player_Click);
            // 
            // PlayerGreen
            // 
            this.PlayerGreen.Name = "PlayerGreen";
            this.PlayerGreen.Size = new System.Drawing.Size(152, 26);
            this.PlayerGreen.Text = "Зелёный";
            this.PlayerGreen.Click += new System.EventHandler(this.Player_Click);
            // 
            // PlayerBlue
            // 
            this.PlayerBlue.Checked = true;
            this.PlayerBlue.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PlayerBlue.Name = "PlayerBlue";
            this.PlayerBlue.Size = new System.Drawing.Size(152, 26);
            this.PlayerBlue.Text = "Синий";
            this.PlayerBlue.Click += new System.EventHandler(this.Player_Click);
            // 
            // цветИгрока2ToolStripMenuItem
            // 
            this.цветИгрока2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EnemyRed,
            this.EnemyYellow,
            this.EnemyGreen,
            this.EnemyBlue});
            this.цветИгрока2ToolStripMenuItem.Name = "цветИгрока2ToolStripMenuItem";
            this.цветИгрока2ToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.цветИгрока2ToolStripMenuItem.Text = "Цвет игрока 2";
            // 
            // EnemyRed
            // 
            this.EnemyRed.Checked = true;
            this.EnemyRed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnemyRed.Name = "EnemyRed";
            this.EnemyRed.Size = new System.Drawing.Size(142, 26);
            this.EnemyRed.Text = "Красный";
            this.EnemyRed.Click += new System.EventHandler(this.Enemy_Click);
            // 
            // EnemyYellow
            // 
            this.EnemyYellow.Name = "EnemyYellow";
            this.EnemyYellow.Size = new System.Drawing.Size(142, 26);
            this.EnemyYellow.Text = "Жёлтый";
            this.EnemyYellow.Click += new System.EventHandler(this.Enemy_Click);
            // 
            // EnemyGreen
            // 
            this.EnemyGreen.Name = "EnemyGreen";
            this.EnemyGreen.Size = new System.Drawing.Size(142, 26);
            this.EnemyGreen.Text = "Зелёный";
            this.EnemyGreen.Click += new System.EventHandler(this.Enemy_Click);
            // 
            // EnemyBlue
            // 
            this.EnemyBlue.Name = "EnemyBlue";
            this.EnemyBlue.Size = new System.Drawing.Size(142, 26);
            this.EnemyBlue.Text = "Синий";
            this.EnemyBlue.Click += new System.EventHandler(this.Enemy_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1082, 608);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "nTRON";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem играToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаяИграToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem паузаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скоростьИгрыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuSpeed5;
        private System.Windows.Forms.ToolStripMenuItem menuSpeed2;
        private System.Windows.Forms.ToolStripMenuItem menuSpeed10;
        private System.Windows.Forms.ToolStripMenuItem menuSpeed15;
        private System.Windows.Forms.ToolStripMenuItem menuSpeed20;
        private System.Windows.Forms.ToolStripMenuItem цветИгрока1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem цветИгрока2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PlayerRed;
        private System.Windows.Forms.ToolStripMenuItem PlayerYellow;
        private System.Windows.Forms.ToolStripMenuItem PlayerGreen;
        private System.Windows.Forms.ToolStripMenuItem PlayerBlue;
        private System.Windows.Forms.ToolStripMenuItem EnemyRed;
        private System.Windows.Forms.ToolStripMenuItem EnemyYellow;
        private System.Windows.Forms.ToolStripMenuItem EnemyGreen;
        private System.Windows.Forms.ToolStripMenuItem EnemyBlue;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}

